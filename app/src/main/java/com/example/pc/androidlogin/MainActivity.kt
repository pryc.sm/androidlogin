package com.example.pc.androidlogin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_goLoginPage.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        btn_goRegisterPage.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

    }

}
