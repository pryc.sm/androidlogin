package com.example.pc.androidlogin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils.isEmpty
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_member.*

class MemberActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member)

        DataFromFacebook()

        btn_memberDone.setOnClickListener {
            checkFormatMember()
        }
    }

    private fun checkFormatMember() {
        val firstname = member_FirstName.text.toString()
        val lastname = member_LastName.text.toString()
        val citizen = member_Citizen.text.toString()
        val mobile = member_Mobile.text.toString()
        val password = member_Password.text.toString()
        val confirmpassword = member_ConfirmPassword.text.toString()

        if (validate(firstname, lastname, citizen, mobile, password, confirmpassword)) {
            val intent = Intent(this, TenantHomeActivity::class.java)
            intent.putExtra("firstname", firstname)
            intent.putExtra("lastname", lastname)
            intent.putExtra("citizen", citizen)
            intent.putExtra("mobile", mobile)
            startActivity(intent)
        }
    }

    private fun validate(firstname: String, lastname: String, citizen: String, mobile: String,
    password: String, confirmpassword: String): Boolean {

        txtInput_memberFirstName.error = null
        txtInput_memberLastName.error = null
        txtInput_memberCitizen.error = null
        txtInput_memberMobile.error = null
        txtInput_memberPassword.error = null
        txtInput_memberConfirmPassword.error = null

        when {
            isEmpty(firstname) -> {
                txtInput_memberFirstName.error = "Please fill in the required fields"
                return false
            }
            isEmpty(lastname) -> {
                txtInput_memberLastName.error = "Please fill in the required fields"
                return false
            }
            isEmpty(password) -> {
                txtInput_memberPassword.error = "Please fill in the required fields"
                return false
            }
            isEmpty(confirmpassword) -> {
                txtInput_memberConfirmPassword.error = "Please fill in the required fields"
                return false
            }
            !isCitizenValid(citizen) -> {
                txtInput_memberCitizen.error = "Please fill in the required fields"
                return false
            }
            !isMobileValid(mobile) -> {
                txtInput_memberMobile.error = "Please fill in the required fields"
                return false
            }
            !isPasswordValid(password) -> {
                txtInput_memberPassword.error = "Password should be a min. 6 chars"
                return false
            }
            !isConfirmPasswordValid(confirmpassword,password) -> {
                txtInput_memberConfirmPassword.error = "Password Not matching"
                return false
            }
        }
        return true
    }

    private fun isCitizenValid(citizen: String): Boolean {
        return citizen.length == 13
    }

    private fun isMobileValid(mobile: String): Boolean {
        return mobile.length == 10
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length in 6..20
    }

    private fun isConfirmPasswordValid(confirmpassword: String, password: String): Boolean {
        return password == confirmpassword
    }

    private fun DataFromFacebook(){
        val firstname_facebook: String = intent.getStringExtra("firstname_facebook")
        member_FirstName.setText(firstname_facebook)

        val lastname_facebook: String = intent.getStringExtra("lastname_facebook")
        member_LastName.setText(lastname_facebook)
    }

}
