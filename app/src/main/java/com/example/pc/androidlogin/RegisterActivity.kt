package com.example.pc.androidlogin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.text.TextUtils.isEmpty
import android.util.Log
import android.util.Patterns
import com.facebook.*
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlin.random.Random

class RegisterActivity : AppCompatActivity() {

    private var callbackManager: CallbackManager? = null
    private var accessToken: AccessToken? = null
    private var profile: Profile? = null
    private var firstname_facebook: String? = ""
    private var lastname_facebook: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        btn_registerEmail.setOnClickListener {
            checkFomatEmail()
        }

        btn_registerFacebook.setOnClickListener {
            registerFacebook()
        }

    }

    private fun checkFomatEmail() {
        val email = register_etEmail.text.toString()

        if (validate(email)) {
            val intent = Intent(this, VerificationActivity::class.java)
            intent.putExtra("email", email)
            startActivity(intent)
        }
    }

    private fun validate(email: String): Boolean {

        editTxt_register_email.error = null

        when {
            isEmpty(email) -> {
                editTxt_register_email.error = "Email is required"
                return false
            }
            !isEmailValid(email) -> {
                editTxt_register_email.error = "Invalid email format"
                return false
            }
        }

        return true
    }

    private fun isEmailValid(email: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun registerFacebook() {

        callbackManager = CallbackManager.Factory.create()

        btn_registerFacebook.setReadPermissions("email", "public_profile")
        btn_registerFacebook.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                accessToken = AccessToken.getCurrentAccessToken()
                profile = Profile.getCurrentProfile()
                firstname_facebook = profile?.firstName
                lastname_facebook = profile?.lastName
            }

            override fun onCancel() {
                Log.d("MainActivity", "Facebook onCancel.")
            }

            override fun onError(error: FacebookException) {
                Log.d("MainActivity", "Facebook onError.")
            }
        })
    }

    private fun DataFacebook() {
        val intent = Intent(this, MemberActivity::class.java)
        intent.putExtra("firstname_facebook", firstname_facebook)
        intent.putExtra("lastname_facebook", lastname_facebook)
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        DataFacebook()
    }
}
