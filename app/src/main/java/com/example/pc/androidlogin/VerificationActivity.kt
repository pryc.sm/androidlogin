package com.example.pc.androidlogin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_verification.*
import kotlin.random.Random

class VerificationActivity : AppCompatActivity() {

    private var code :String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)

        randomVerificationCode()

        var email: String = intent.getStringExtra("email")
        txt_verification_title.text = "We have sent a verification code to $email. Please enter the code to verify your account."

        txt_verification_resendCode.setOnClickListener {
            randomVerificationCode()
        }

        btn_verification_sendCode.setOnClickListener {
            var codeTest = verification_etVerificationCode.text.toString()

            if (codeTest == code) {
                startActivity(Intent(this, MemberActivity::class.java))
            } else {
                Toast.makeText(this, "Fail!!!!!!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun randomVerificationCode() {
        var verificationCode = String.format("%04d", Random.nextInt(10000))
        Log.d("VerificationActivity", "Verification Code : $verificationCode")
        code = verificationCode

    }
}

