package com.example.pc.androidlogin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils.isEmpty
import android.util.Log
import kotlinx.android.synthetic.main.activity_login.*
import android.util.Patterns
import com.facebook.*
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.activity_member.*
import android.R.id.edit
import android.content.SharedPreferences
import android.preference.PreferenceManager



class LoginActivity : AppCompatActivity() {

    private val email_test: String = "test@email.com"
    private val password_test: String = "123456"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_login.setOnClickListener {
            checkAccount_email_password()
        }

        txt_forgotPassword.setOnClickListener {
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        }

    }

    private fun checkAccount_email_password() {

        val email = etEmail.text.toString()
        val password = etPassword.text.toString()

        if (validate(email, password)) {
            if (email == email_test && password == password_test) {
                startActivity(Intent(this, TenantHomeActivity::class.java))
            } else {
                editTxt_password.error = "Email or Password is incorrect."
            }
        }

    }

    private fun validate(email: String, password: String): Boolean {

        editTxt_email.error = null
        editTxt_password.error = null

        when {
            isEmpty(email) -> {
                editTxt_email.error = "Email is required"
                return false
            }
            isEmpty(password) -> {
                editTxt_password.error = "Password is required"
                return false
            }
            !isEmailValid(email) -> {
                editTxt_email.error = "Invalid email format"
                return false
            }
            !isPasswordValid(password) -> {
                editTxt_password.error = "Password must contain at least 6 characters"
                return false
            }
        }
        return true

    }

    private fun isEmailValid(email: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length in 6..20
    }

}


