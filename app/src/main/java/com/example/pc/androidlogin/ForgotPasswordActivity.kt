package com.example.pc.androidlogin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.text.TextUtils.isEmpty
import android.util.Patterns
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        btn_continue.setOnClickListener {
            checkFormatEmail()
        }
    }

    private fun checkFormatEmail(){

        val email = etEmail.text.toString()

        if (validate(email)) {
                Toast.makeText(this,"set password your email!!", Toast.LENGTH_SHORT).show()
        }

    }

    private fun validate(email: String): Boolean {

        txt_input_email.error = null

        when {
            isEmpty(email) -> {
                txt_input_email.error = "Email is required"
                return false
            }
            !isEmailValid(email) -> {
                txt_input_email.error = "Invalid email format"
                return false
            }
        }

        return true
    }

    private fun isEmailValid(email: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

}
