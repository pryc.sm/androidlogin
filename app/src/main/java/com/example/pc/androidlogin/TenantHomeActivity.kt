package com.example.pc.androidlogin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_tenant_home.*

class TenantHomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tenant_home)

        showDetailMember()
    }

    private fun showDetailMember() {
        val firstname: String = intent.getStringExtra("firstname")
        val lastname: String = intent.getStringExtra("lastname")
        val citizen: String = intent.getStringExtra("citizen")
        val mobile: String = intent.getStringExtra("mobile")

        txt_name.text = "$firstname $lastname"
        txt_citizen.text = citizen
        txt_mobile.text = mobile
    }
}
